# clear results file
> $2

while read -r x; do
    c=2
    max_c=$x
    i=1
    ans="$x: "
    while [[ $c -le $max_c ]]; do
        r=$[x % c]
        if [[ $r -eq 0 ]]
        then
            x=$[x/c]
            ans="$ans $c"
        else
            c=$[c+1]
        fi
    done
    echo $ans >> $2
done < $1
