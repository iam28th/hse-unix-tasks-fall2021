read a

b=$(echo $a | rev)

flag=1
for (( i=0; i<${#a}; i++ )); do
    cha=${a:$i:1}
    chb=${b:$i:1}
    if [[ $cha != $chb ]]
    then
        flag=0
        break
    fi
done

if [[ $flag == 1 ]]
then
    echo YES
else
    echo NO
fi
