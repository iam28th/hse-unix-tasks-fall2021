find . -type f -not -name "*.sh" -ctime -2 -ctime 0 -print0 |
    while IFS='\0' read -r -d '' oldname
    do
        bname=$(basename $oldname)
        dir=$(dirname $oldname)
        newname="$dir/_$bname"

        mv $oldname $newname
    done
