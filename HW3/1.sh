while read -r line; do
    output=$(ping -c 1 "$line")
    if [[ $? -eq 0 ]]
    then
        echo $output >> res.txt
    else
        echo $output >> err.txt
    fi
done < ips.txt
